# docker-elasticsearch

Builds Link's elasticsearch docker image

Designed to be used with [Center for Digital Resilience's version of
Zammad](https://gitlab.com/digiresilience/link/zammad).


## Developer Notes

### Building

### Simple

```bash
make DOCKER_TAG=latest
```

Creates an image `digiresilience/link/docker-elasticsearch:latest`

### You special snowflake you

```bash
make DOCKER_TAG=latest DOCKER_NS=batman/elasticsearch
```
Creates the image `batman/elasticsearch:myspecialversion`

## License

This project is licensed under the [GNU Affero General Public License (AGPL)
v3+](https://www.gnu.org/licenses/agpl-3.0.en.html).

## Maintainers

* Abel Luck <abel@guardianproject.info> of [Guardian Project](https://guardianproject.info)

