FROM docker.elastic.co/elasticsearch/elasticsearch:7.6.0
LABEL maintainer="Abel Luck <abel@guardianproject.info>"
ARG BUILD_DATE

LABEL org.label-schema.build-date="$BUILD_DATE" \
      org.label-schema.name="elasticsearch" \
      org.label-schema.license="AGPL-3.0" \
      org.label-schema.description="Docker container for Link - Elasticsearch" \
      org.label-schema.url="https://digiresilience.org" \
      org.label-schema.vcs-url="https://gitlab.com/digiresilience/link/docker-elasticsearch" \
      org.label-schema.vcs-type="Git" \
      org.label-schema.vendor="CDR" \
      org.label-schema.schema-version="2.9.0"

RUN yes | /usr/share/elasticsearch/bin/elasticsearch-plugin install ingest-attachment
